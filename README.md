
## 安装

```bash
$ npm i -g smart-npm # 安装 snpm
$ snpm i #注意环境变量设置
```
## 运行

```bash
$ npm run dev
```
## 介绍

```bash
1这是一个基于微信端的单页应用，使用到的技术栈有vue（js框架），vux(ui框架)，vue-router(路由)，VueResource(http插件),适合初学者学习
2 微信里面坑很多这里有个网页授权的txt文件，通过webpack打包后会放到根目录的

```
## 目录架构
```bash
build	项目构建(webpack)相关代码
config	配置目录，包括端口号等。我们初学可以使用默认的。
node_modules	npm 加载的项目依赖模块，snpm i后产生
src	
包含了几个目录及文件：
	assets: 放置图片，如logo，菜单图片等等。
	components: 目录里面放了一个组件文件，可以不用。
	router: 路由跳转
	services: 与后台交互API配置
	store: 状态管理
	vue: 展示使用的vue文件
	App.vue: 项目入口文件，我们也可以直接将组件写这里，而不使用 components 目录。
	main.js: 项目的核心文件。
static	静态资源目录，如图片、字体等。
test	初始测试目录，可删除
.xxxx文件	这些是一些配置文件，包括语法配置，git配置等。
index.html	首页入口文件，你可以添加一些 meta 信息或统计代码啥的。
package.json	项目配置文件。
README.md	项目的说明文档，markdown 格式

```
## 公众号测试：
```bash
启动内网穿透工具：小米球Ngrok启动工具.bat
前缀名：wx
端口：9999,端口根据配置使用
公众号菜单点击：UI测试
后台没做处理，微信平台会视为失败，所以可能会打开很慢，甚至无显示
```